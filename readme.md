# Fold

![](http://i1117.photobucket.com/albums/k594/thetutlage/poppins-1_zpsg867sqyl.png)
![](https://img.shields.io/travis/poppinss/adonis-fold.svg)

[![Coverage Status](https://coveralls.io/repos/poppinss/adonis-fold/badge.svg?branch=master&service=github)](https://coveralls.io/github/poppinss/adonis-fold?branch=master)

Fold is an Ioc container for Node applications borrowed ideas from Php and has support for 

1. Service Provider
2. Service Manager
3. Namespace/Auto Loading

Ioc container enables solid Dependency Injection through out your application and makes it easier for you to test your modules by mocking dependencies with ease.

Fold is used by [Adonis](http://adonisjs.com) and you should checkout [Ioc Container Documentation](#http://adonisjs.com/docs/1.0/ioc-container)

## License
( The MIT License )

Copyright (c) 2015 Poppins

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
